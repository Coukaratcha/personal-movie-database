import os

class Config():
	SQLALCHEMY_ECHO = os.environ.get("SQLALCHEMY_ECHO") in ("True", "true")
	SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get("SQLALCHEMY_TRACK_MODIFICATIONS") in ("True", "true")
	SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")

	SECRET_KEY = os.environ.get("SECRET_KEY")
	SESSION_COOKIE_NAME = os.environ.get("SESSION_COOKIE_NAME")