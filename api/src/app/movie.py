from flask import (
	Blueprint,
	request,
	abort,
	jsonify
)

from app.models.movie import Movie, Version

from app.db import db

movie = Blueprint(__name__, __name__)

@movie.route("/", methods=["GET", "POST"])
def root():
	if request.method == "GET":
		movies = Movie.query.all()

		return jsonify([m.json for m in movies])
	elif request.method == "POST":
		data = request.json

		m = Movie(
			imdbID=data["imdbID"],
			title=data["title"],
			year=data["year"],
			score=data["score"],
			scoreIMDB=data["scoreIMDB"],
			version=data["version"],
			watch_year = data["watch_year"] if data["watch_year"] else None,
			watch_month = data["watch_month"] if data["watch_month"] and data["watch_year"] else None,
			watch_day = data["watch_day"] if data["watch_day"] and data["watch_month"] and data["watch_year"] else None,
			inTheatre=data["inTheatre"],
			isFrench=data["isFrench"]
		)

		db.session.add(m)
		db.session.commit()

		return jsonify(m.json)

@movie.route("/<movie_id>", methods=["GET", "POST", "DELETE"])
def root_id(movie_id):
	m = Movie.query.get(movie_id)

	if not m:
		abort(404)

	if request.method == "GET":
		return jsonify(m.json)
	elif request.method == "POST":
		data = request.json

		m.imdbID = data["imdbID"]
		m.title = data["title"]
		m.year = data["year"]
		m.score = data["score"]
		m.scoreIMDB = data["scoreIMDB"]
		m.version = data["version"]
		watch_year = data["watch_year"] if data["watch_year"] else None
		watch_month = data["watch_month"] if data["watch_month"] and data["watch_year"] else None
		watch_day = data["watch_day"] if data["watch_day"] and data["watch_month"] and data["watch_year"] else None
		m.inTheatre = data["inTheatre"]
		m.isFrench = data["isFrench"]

		db.session.commit()

		return jsonify(m.json)
	elif request.method == "DELETE":
		db.session.delete(m)
		db.session.commit()

		return jsonify(m.json)

@movie.route("/score")
def score():
	scores = db.session.query(Movie.score, db.func.count(Movie.score)).group_by(Movie.score).all()

	res = [{
		"score": i,
		"value": 0
	} for i in range(11)]

	for s in scores:
		res[s[0]]["value"] = s[1]

	return jsonify(res)