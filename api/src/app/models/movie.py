from app import db

import enum

class Version(enum.Enum):
	VF = "VF"
	VO = "VO"

class Movie(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	imdbID = db.Column(db.String(80), nullable=False)
	title = db.Column(db.String(80), nullable=False)
	year = db.Column(db.Integer, nullable=False)
	score = db.Column(db.Integer, nullable=False)
	scoreIMDB = db.Column(db.Float, nullable=False)
	version = db.Column(db.Enum(Version), nullable=False)
	watch_day = db.Column(db.Integer, nullable=True)
	watch_month = db.Column(db.Integer, nullable=True)
	watch_year = db.Column(db.Integer, nullable=True)
	inTheatre = db.Column(db.Boolean, nullable=False)
	isFrench = db.Column(db.Boolean, nullable=False)

	def __repr__(self):
		return "<Movie {n} ({y})>".format(n=self.title, y=self.year)

	@property
	def json(self):
		return {
			"id": self.id,
			"imdbID": self.imdbID,
			"title": self.title,
			"year": self.year,
			"score": self.score,
			"scoreIMDB": self.scoreIMDB,
			"version": "FR" if self.isFrench else ("VF" if self.version == Version.VF else "VO"),
			"date": '/'.join([str(x).zfill(2) for x in [self.watch_year, self.watch_month, self.watch_day] if x]),
			"inTheatre": self.inTheatre
		}