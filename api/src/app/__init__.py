from flask import (
	Flask,
	request,
	abort
)

from app.db import db

from app.movie import movie

app = Flask(__name__)
app.config.from_object("app.config.Config")

app.register_blueprint(movie, url_prefix="/movie")

@app.before_request
def require_apikey():
	API_KEY = app.config["SECRET_KEY"]
	if not (request.headers.get("api-key") and request.headers.get("api-key") == API_KEY):
		abort(401, "Invalid API key")

db.init_app(app)

with app.app_context():
	db.create_all()