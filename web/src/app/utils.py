from flask import (
	session,
	abort,
	current_app
)

from functools import wraps

headers = {
	"Content-Type": "application/json"
}

def require_session(view_function):
	@wraps(view_function)
	def decorated_function(*args, **kwargs):
		if "login" not in session:
			abort(401)
		else:
			return view_function(*args, **kwargs)
	return decorated_function