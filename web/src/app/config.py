import os

class Config():
	SECRET_KEY = os.environ.get("SECRET_KEY")
	SESSION_COOKIE_NAME = os.environ.get("SESSION_COOKIE_NAME")

	PASSWORD = os.environ.get("PASSWORD")

	API_URL = os.environ.get("API_URL")
	API_KEY = os.environ.get("API_KEY")

	OMDB_API_URL = os.environ.get("OMDB_API_URL")
	OMDB_API_KEY = os.environ.get("OMDB_API_KEY")