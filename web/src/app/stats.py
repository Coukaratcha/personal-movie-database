from flask import (
	Blueprint,
	jsonify,
	current_app
)

import requests

from app.utils import require_session, headers

stats = Blueprint(__name__, __name__)

@stats.route("/scores", methods=["GET"])
def score():
	url = "{base}/movie/score".format(base=current_app.config["API_URL"])

	r = requests.get(url, headers=headers)

	return jsonify(r.json())