from flask import (
	Blueprint,
	jsonify,
	current_app
)

import requests

from app.utils import require_session, headers

omdb = Blueprint(__name__, __name__)

@omdb.route("/search/<search>", methods=["GET"])
@require_session
def search(search):
	url = "{base}/?apikey={key}&type=movie&s={search}".format(base=current_app.config["OMDB_API_URL"], key=current_app.config["OMDB_API_KEY"], search=search)

	r = requests.get(url)

	return jsonify(r.json())

@omdb.route("/id/<imdbID>", methods=["GET"])
@require_session
def fetch(imdbID):
	url = "{base}/?apikey={key}&type=movie&i={i}".format(base=current_app.config["OMDB_API_URL"], key=current_app.config["OMDB_API_KEY"], i=imdbID)

	r = requests.get(url)

	return jsonify(r.json())