$(function(){
	update();
});

function update_scores(data){
	$("#scores").empty();
	
	var margin = {top: 10, right: 30, bottom: 30, left: 40},
		width = $("#scores").width() - margin.left - margin.right,
		height = 500 - margin.top - margin.bottom;

	var svg = d3.select("#scores")
		.append("svg")
			.attr("width", "100%")
			.attr("height", height + margin.top + margin.bottom)
		.append("g")
			.attr("transform",
						`translate(${margin.left}, ${margin.top})`);

	var x = d3.scaleBand()
		.range([0, width])
		.domain(data.map( x => x.score))
		.padding(0);

	svg.append("g")
		.attr("transform", `translate(0, ${height})`)
		.call(d3.axisBottom(x).tickSizeOuter(0))
		.select(".domain")
		.remove()

	var y = d3.scaleLinear()
		.range([height, 0])
		.domain([0, Math.max(...data.map( x => x.value))]);

	console.log( Array.from(Array(Math.max(...data.map(x=>x.value))), (_, i)=>i).slice(1));
	
	svg.append("g")
		.call(d3.axisLeft(y).tickValues( Array.from(Array(Math.max(...data.map(x=>x.value))), (_, i)=>i+1) ))
		.select(".domain")
		.remove();

	svg.selectAll("mybar")
		.data(data)
		.enter()
		.append("rect")
			.attr("x", d => x(d.score))
			.attr("y", d => y(d.value))
			.attr("width", x.bandwidth())
			.attr("height", d => (height - y(d.value)))
			.attr("fill", "#69b3a2");
}

function update(){
	$.get({
		url: "/stats/scores",
		dataType: "json"
	})
	.done(update_scores);
}