$(function(){
	update();
});

function update(){
	$.get({
		url: "/get_movies",
		contentType: "json"
	})
	.done(update_movies);
}

function update_movies(data){
	$("table.table tbody").empty();

	for (m of data){
		$("table.table")
		.append(`<tr>\
		<td>${m.title}</td>\
		<td>${m.year}</td>\
		<td>${m.score}</td>\
		<td>${m.scoreIMDB}</td>\
		<td>${m.version}</td>\
		<td>${m.date}</td>\
		<td><span class="badge badge-${m.inTheatre ? "success" : "danger"}">${m.inTheatre ? "Oui" : "Non"}</span></td>\
		</tr>`)
	}
}