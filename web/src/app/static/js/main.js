$(function(){
	let searchParams = new URLSearchParams(window.location.search);

	function fill(data){
		$("#rateModal form#form-rate input#title").val(data["Title"]);
		$("#rateModal form#form-rate input#year").val(data["Year"]);
		$("#rateModal form#form-rate input#scoreIMDB").val(data["imdbRating"]);
		$("#rateModal form#form-rate input#imdbID").val(data["imdbID"]);

		var isFrench = data["Language"].split(',')[0] == "French"

		$("#rateModal form#form-rate input#isFrench").prop("checked", isFrench);
		$("#rateModal ul#list-search").empty();
	}
	
	if (searchParams.has("login_error")){
		$("#form-login input#password")
		.addClass("is-invalid")
		.next()
		.text(searchParams.get("login_error"));

		$("#loginModal").modal("show");
	}

	$("#rateModal #form-search-id").submit(function(e){
		e.preventDefault();

		var search = $("#form-search-id input#search-id").val();

		$.get({
			url: `/omdb/id/${search}`,
			dataType: "json"
		})
		.done(fill);
	});

	$("#rateModal #form-search-title").submit(function(e){	
		e.preventDefault();

		var search = $("#form-search-title input#search-title").val();

		$.get({
			url: `/omdb/search/${search}`,
			dataType: "json"
		})
		.done(function(data){
			var list = $("#rateModal ul#list-search");

			list.empty();

			for (x of data["Search"]){
				list.append(`<li data-id="${x["imdbID"]}" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">\
				${x["Title"]} (${x["Year"]})\
				<img src="${x["Poster"]}" class="poster">
				</li>`);
			}

			$("#rateModal ul#list-search li").click(function () {
				var id = $(this).attr("data-id");

				$.get({
					url: `/omdb/id/${id}`,
					dataType: "json"
				})
					.done(fill);
			});
		});
	});

	$("#rateModal form#form-rate").submit(function(e){
		e.preventDefault();

		var imdbID, title, year, score, scoreIMDB, version, watch_year, watch_month, watch_day, inTheatre, isFrench;

		imdbID = $("#rateModal form#form-rate input#imdbID").val();
		title = $("#rateModal form#form-rate input#title").val();
		year = parseInt($('#rateModal form#form-rate input#year').val());
		score = parseInt($("#rateModal form#form-rate select#score").val());
		scoreIMDB = parseFloat($("#rateModal form#form-rate input#scoreIMDB").val());
		version = $("#rateModal form#form-rate select#version").val();
		watch_year = parseInt($("#rateModal form#form-rate input#watch_year").val());
		watch_month = parseInt($("#rateModal form#form-rate select#watch_month").val());
		watch_day = parseInt($("#rateModal form#form-rate input#watch_day").val());
		inTheatre = $("#rateModal form#form-rate input#inTheatre").prop("checked");
		isFrench = $("#rateModal form#form-rate input#isFrench").prop("checked");

		$.post({
			url: "/rate",
			data: JSON.stringify({
				imdbID: imdbID,
				title: title,
				year: year,
				score: score,
				scoreIMDB: scoreIMDB,
				version: version,
				watch_year: watch_year,
				watch_month: watch_month,
				watch_day: watch_day,
				inTheatre: inTheatre,
				isFrench: isFrench
			}),
			contentType: "application/json",
			dataType: "json"
		})
		.done(function(data){
			$("#rateModal").modal("hide");
			update();
		});
	});
});