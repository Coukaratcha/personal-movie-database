from flask import (
	Flask,
	render_template,
	request,
	jsonify,
	abort,
	current_app,
	session,
	redirect,
	url_for
)

import requests, datetime

from functools import wraps

from app.omdb import omdb
from app.stats import stats

from app.utils import require_session, headers

app = Flask(__name__)
app.config.from_object("app.config.Config")

app.register_blueprint(omdb, url_prefix="/omdb")
app.register_blueprint(stats, url_prefix="/stats")

@app.before_request
def set_headers():
	headers["api-key"] = current_app.config["API_KEY"]

@app.route("/")
@app.route("/data")
def home():
	return render_template("home.html")

@app.route("/score")
def score():
	return render_template("score.html")

@app.route("/login", methods=["POST"])
def login():
	data = request.form

	if data["password"] == current_app.config["PASSWORD"]:
		session["login"] = True
		return redirect(url_for("home"))
	
	return redirect(url_for("home", login_error = "Invalid credentials"))

@app.route("/logout")
def logout():
	session.pop("login", None)
	return redirect(url_for("home"))

@app.route("/get_movies", methods=["GET"])
def get_movies():
	url = "{base}/movie/".format(base=current_app.config["API_URL"])

	r = requests.get(url, headers=headers)
	res = r.json()

	return jsonify(res)

@app.route("/rate", methods=["POST"])
@require_session
def rate():
	data = request.json

	if not ("title" in data and data["title"]):
		abort(400)
	
	if not ("year" in data and data["year"] in range(1000, 3000)):
		abort(400)

	if not ("scoreIMDB" in data and data["scoreIMDB"] >= 0 and data["scoreIMDB"] <= 10):
		abort(400)
	
	if not ("score" in data and data["score"] in range(11)):
		abort(400)

	if not ("version" in data and data["version"] in ("VF", "VO")):
		abort(400)

	if not ("inTheatre" in data):
		abort(400)

	if not ("isFrench" in data):
		abort(400)

	if not ("watch_year" in data and data["watch_year"] and isinstance(data["watch_year"], int)):
		data["watch_year"] = None
		data["watch_month"] = None
		data["watch_day"] = None
	elif not ("watch_month" in data and data["watch_month"] in range(1,13)):
		data["watch_month"] = None
		data["watch_day"] = None
	elif not ("watch_day" in data and data["watch_day"] in range(1,32)):
		data["watch_day"] = None
	else:
		try:
			datetime.datetime(data["watch_year"], data["watch_month"], data["watch_day"])
		except:
			abort(400)

	url = "{base}/movie/".format(base=current_app.config["API_URL"])

	r = requests.post(url, headers = headers, json = data)

	return jsonify(r.json())

@app.route("/delete_movie/<movie_id>", methods=["POST"])
@require_session
def delete_movie(movie_id):
	url = "{base}/movie/{m_id}".format(base=current_app.config["API_URL"], m_id=movie_id)

	r = requests.delete(url, headers = headers)

	return jsonify(r.json())